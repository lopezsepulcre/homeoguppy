---
title: "Condition"
author: "Amina Mohamed"
date: "3/12/2024"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(ggplot2)
library(tidyverse)
library(sjPlot)
library(sjlabelled)
library(sjmisc)
library(ggeffects)
```



Fulton's Condition factor K is defined as $K-factor = [Wet mass (g)/standard length (cm)^3] × 100$

```{r}
hom<-read_csv('../data_raw/Treatmentfishes.csv')
hom$Treatment<-as.factor(hom$Treatment)
hom$Pred<-as.factor(hom$Pred)

hom<-hom %>% 
  mutate(length=Standard_length_mm/10) %>%   # to convert it to cm
  mutate(Cond=100*Wetmassg/(length^3)) %>%  # with just wet mass
  mutate(Cond0=100*Wetmass_wo_io_g/(length^3)) %>% # with wet mass without internal organs
  mutate(cond1=100*Drymassg/(length^3)) # with dry mass

ggplot(data=hom,aes(y=Cond0, x=Treatment, color=Treatment))+geom_boxplot()+theme_classic()+geom_point()
ggplot(data=hom, aes(y=Cond0, x=Pred, color=Pred))+geom_boxplot()+theme_classic()+geom_point()



```
They look similar. Let us check if there is any difference.

```{r}

lin<-lm(data=hom, Cond0 ~ Pred + Treatment + Pred*Treatment)
summary(lin)
hom$Pred<-relevel(hom$Pred, ref="LP")

lin<-lm(data=hom, Cond0 ~ Pred + Treatment + Pred*Treatment)
summary(lin)

```
No significant effect of predation or treatment.

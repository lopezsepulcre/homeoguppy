---
title: "Yarra"
author: "Amina Mohamed"
date: "`r Sys.Date()`"
output: html_document
---

```{r setup, include=FALSE}
library(knitr)
  opts_chunk$set(echo = TRUE, warning = FALSE, message = FALSE)

library(tidyverse)
library(ggeffects)
library(MASS)
library(glmmTMB)
```



The main data for this experiment (body stoichiometry and excretion) is stored in `Yarran.csv`, which we will load, ensure the factors are treated as such, and rename variables with cumbersome names: 

```{r}
datay <- read_csv('../data_raw/Yarra.csv')%>%
  mutate(Treatment = factor(Treatment),
         Pred = factor(Pred),
         Parent = factor(Parent),
         Stream = factor(Stream),
         Block = factor(Date_setup),
         Dead = factor(Dead))%>%
  rename(SL = finalSLmm,
         dry_mass = drymassg)
```
In order to later analyze nutrient imbalance, we also need the values of CN and CP for high and low quality diets:

```{r}
CN_LQ = 10.40*1.166
CP_LQ = 355.08*2.579
CN_HQ = 5.41*1.166
CP_HQ = 126.62*2.579
```

P curve
```{r}
linP<-lm(avabs~`p conc`, data=datay)

summary(linP)
int<-summary(linP)$coefficients[1,1]
slo<- summary(linP)$coefficients[2,1]
```

```{r}
datay <-datay %>% 
  mutate(Pfish=((Pabs-int)/slo)*20) %>% 
  mutate(weighpmg=BodyPweight*1000) %>% 
  mutate(PerP=(Pfish/weighpmg)*100*0.012) %>% 
  mutate(CN =CN*1.166) %>% 
  mutate(CP = (PerC/PerP)*2.579)
            
``` 

  

Now we can calculate the new imbalance variables as wells as the C:element ratios:
```{r}
datay <-datay %>% 
  mutate(food_CN = case_when(Treatment == 'Algae like' ~  CN_LQ,
                             Treatment == 'Invert like' ~ CN_HQ),
         food_CP = case_when(Treatment == 'Algae like' ~  CP_LQ,
                             Treatment == 'Invert like' ~ CP_HQ)) %>%
  mutate(imbalN = food_CN - CN) %>% 
   mutate(imbalP = food_CP - CP)

```


### Body Stoichiometry (% element)

In this analysis we ask whether there are differences in the homeostatic capabilities of guppies from different predation regimes by testing if the interaction between food treatment and predation is significant. Because stoichiometry is often size-dependent, and this homeostasis may change with size, we include also size (standard length). We start with a model with 3-way interaction and remove non-significant interactions.

As random effects, we include parent and experimental block (setup time).

```{r}
model_bodyNY <- glmmTMB(log(PerN) ~ (log(SL) + Pred + Treatment) +
                         (1|Block) + (1|Parent), datay)

summary(model_bodyNY)$coefficients
```
Let us plot this with data points in the background
```{r,fig.width=3.5, fig.height=4}
pred_bodyNY <- ggpredict(model_bodyNY, terms = c("Treatment", "Pred"))

plothomNY<-ggplot(data = pred_bodyNY, aes(x = x, y = predicted, group = group)) +
  geom_point(data =datay, aes(y=PerN, x= Treatment, group = Pred),
            alpha = 0.5, col = 'gray70', shape = '_', size = 5,
             position = position_dodge(width = 0.25)) +
    geom_line(aes(x = x, y = predicted,shape = group, lty = group),
             position = position_dodge(width = 0.25)) +
  geom_errorbar(aes(ymin = conf.low, 
                    ymax = conf.high),
                position = position_dodge(width = 0.25), 
                width = 0.1) +
   geom_point(aes(x = x, y = predicted, shape = group),
             size = 4.5, fill = 'white', col = 'indianred',
             position = position_dodge(width = 0.25)) +
  scale_y_log10() +
  scale_x_discrete(labels = c("Low",
                              "High")) +
  scale_shape_manual(labels = c('High Predation', 
                                'Low Predation'),
                     values = c(17, 16),
                     name = NULL) +
    scale_linetype_manual(labels = c('High Predation', 
                                'Low Predation'),
                     values = c(1, 2),
                     name = NULL) +
  labs(x = 'Food Quality', 
       y = '%N') +
  theme_bw() +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        strip.background = element_blank(),
        legend.position = 'top',
        text = element_text(size = 14))
plothomNY
```


Now to check for P patterns

```{r}
model_bodyP <- glmmTMB(log(PerP) ~ (log(SL) + Pred + Treatment) +
                         (1|Block) + (1|Parent), datay)

summary(model_bodyP)$coefficients
```
let us look at the data
```{r,fig.width=5, fig.height=6}
pred_bodyP <- ggpredict(model_bodyP, terms = c("Treatment", "Pred"))

plothomP<-ggplot(data = pred_bodyP, aes(x = x, y = predicted, group = group)) +
  geom_point(data =datay, aes(y=PerP, x= Treatment, group = Pred),
            alpha = 0.5, col = 'gray70', shape = '_', size = 5,
             position = position_dodge(width = 0.25)) +
    geom_line(aes(x = x, y = predicted,shape = group, lty = group),
             position = position_dodge(width = 0.25)) +
  geom_errorbar(aes(ymin = conf.low, 
                    ymax = conf.high),
                position = position_dodge(width = 0.25), 
                width = 0.1) +
   geom_point(aes(x = x, y = predicted, shape = group),
             size = 4.5, fill = 'white', col = 'indianred',
             position = position_dodge(width = 0.25)) +
  scale_y_log10() +
  scale_x_discrete(labels = c("Low",
                              "High")) +
  scale_shape_manual(labels = c('High Predation', 
                                'Low Predation'),
                     values = c(17, 16),
                     name = NULL) +
    scale_linetype_manual(labels = c('High Predation', 
                                'Low Predation'),
                     values = c(1, 2),
                     name = NULL) +
  labs(x = 'Food Quality', 
       y = '%P') +
  theme_bw() +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        strip.background = element_blank(),
        legend.position = 'top',
        text = element_text(size = 14))
plothomP
```
```{r}
datay <- datay %>% 
  mutate(CNrev =CNrev*1.166) %>% 
  mutate(CP = (Crev/PerP)*2.579)

```

```{r}
model_bodyNYrev <- glmmTMB(log(Nrev) ~ (log(SL) + Pred + Treatment) +
                         (1|Block) + (1|Parent), datay)

summary(model_bodyNYrev)$coefficients
```

```{r,fig.width=3.5, fig.height=4}
pred_bodyNY <- ggpredict(model_bodyNYrev, terms = c("Treatment", "Pred"))

plothomNY<-ggplot(data = pred_bodyNY, aes(x = x, y = predicted, group = group)) +
  geom_point(data =datay, aes(y=PerN, x= Treatment, group = Pred),
            alpha = 0.5, col = 'gray70', shape = '_', size = 5,
             position = position_dodge(width = 0.25)) +
    geom_line(aes(x = x, y = predicted,shape = group, lty = group),
             position = position_dodge(width = 0.25)) +
  geom_errorbar(aes(ymin = conf.low, 
                    ymax = conf.high),
                position = position_dodge(width = 0.25), 
                width = 0.1) +
   geom_point(aes(x = x, y = predicted, shape = group),
             size = 4.5, fill = 'white', col = 'indianred',
             position = position_dodge(width = 0.25)) +
  scale_y_log10() +
  scale_x_discrete(labels = c("Low",
                              "High")) +
  scale_shape_manual(labels = c('High Predation', 
                                'Low Predation'),
                     values = c(17, 16),
                     name = NULL) +
    scale_linetype_manual(labels = c('High Predation', 
                                'Low Predation'),
                     values = c(1, 2),
                     name = NULL) +
  labs(x = 'Food Quality', 
       y = '%N') +
  theme_bw() +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        strip.background = element_blank(),
        legend.position = 'top',
        text = element_text(size = 14))
plothomNY
```

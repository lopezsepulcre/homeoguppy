## Guppy Stoichiometric Homeostasis Project

[Describe here the project]

## Package Usage

If you are a member of the project but that just wants to see how the analyses were made or reports of the results, you will find those under the Articles tab. If you want to use the package or need to contribute data, documentation, or code you must clone this package and install it in your computer.

This project is version controlled via git hosted in GitLab to enable sharing and synchronization among collaborators.

In order to have a copy of this package in your computer, you must:

Have access rights to the GitLab project repository at https://gitlab.com/lopezsepulcre/homeoguppy

Have git installed and set up in your computer. If you don’t, go to https://git-scm.com to download it.

If you never used git collaboratively, you will probably have to create an SSH key as described [here](http://happygitwithr.com/ssh-keys.html).

To clone the full project repository, type in Terminal:

`git clone git@gitlab.com:lopezsepulcre/homeoguppy.git`


To contribute, you can then open the package with RStudio by clicking on the homeoguppy.Rproj file. Remember to branch, commit and request merges with your code.

To use the package, simply type in RStudio console:

`library(homeoguppy)`
